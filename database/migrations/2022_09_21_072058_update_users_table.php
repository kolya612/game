<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->tinyInteger('type')->default(0);
            $table->integer('intelligence')->default(0);
            $table->integer('dexterity')->default(0);
            $table->integer('power')->default(0);
            $table->integer('accuracy')->default(0);
            $table->integer('healthy')->default(0);
            $table->integer('magic')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('type');
        Schema::dropIfExists('intelligence');
        Schema::dropIfExists('dexterity');
        Schema::dropIfExists('power');
        Schema::dropIfExists('accuracy');
        Schema::dropIfExists('healthy');
        Schema::dropIfExists('magic');
    }
};
