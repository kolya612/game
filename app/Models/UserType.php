<?php

namespace App\Models;

final class UserType
{
    private const TYPES = [
        0 => [
            'title' => 'Knight',
            'features' => [
                'intelligence' => 3, /*интеллект*/
                'dexterity' => 4, /*ловкость*/
                'power' => 8, /*сила*/
                'accuracy' => 5, /*меткость*/
                'healthy' => 100,
                'magic' => 20,
            ]
        ],
        1 => [
            'title' => 'Magician',
            'features' => [
                'intelligence' => 10,
                'dexterity' => 6,
                'power' => 2,
                'accuracy' => 2,
                'healthy' => 80,
                'magic' => 60,
            ]
        ],
        2 => [
            'title' => 'Archer',
            'features' => [
                'intelligence' => 1,
                'dexterity' => 6,
                'power' => 4,
                'accuracy' => 11,
                'healthy' => 90,
                'magic' => 10,
            ]
        ],
        3 => [
            'title' => 'Robber',
            'features' => [
                'intelligence' => 1,
                'dexterity' => 4,
                'power' => 11,
                'accuracy' => 4,
                'healthy' => 120,
                'magic' => 0,
            ]
        ],
    ];

    public static function getTypes(): array
    {
        return array_map(fn(array $v): string => $v['title'], array_values(self::TYPES));
    }

    public static function getTypeFeature(int $id): array
    {
        return self::TYPES[$id]['features'];
    }

    public static function getTypeTitle(int $id): array
    {
        return self::TYPES[$id]['title'];
    }
}
